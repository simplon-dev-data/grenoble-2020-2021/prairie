# Activité base de données SQLite

## Activité de recherche d'informations

Pour travailler avec les données, nous allons utiliser une base de données. Nous avons choisi de commencer par une base de données SQLite.

- A quels grands types de base de données SQLite appartient-elle?
- Trouver d'autres exemples de bases de données faisant partie de cette même catégorie
- Pourquoi avons-nous choisi de commencer par cette base de données ?
- Trouver des exemples de cas d'utilisation d'SQLite

## Activité de mise en place de SQLite

Pour pouvoir utiliser SQLite, il faut d'abord l'installer.

- [ ] Ouvrir un terminal
- [ ] Taper la commande suivante :
```bash
sudo apt-get install sqlite3
```
- [ ] Vérifier que l'installation a bien fonctionné en tapant la commande suivante :
```bash
sqlite3
```
Si tout fonctionne, vous devez voir s'afficher dans le terminal un message de ce type :
```bash
SQLite version 3.31.1 2020-01-27 19:55:54
Enter ".help" for usage hints.
Connected to a transient in-memory database.
Use ".open FILENAME" to reopen on a persistent database.
sqlite>
```

## Activité création d'une table par étapes

Le but de cette activité est de créer une base de données avec une table contenant les données du fichier CSV sur les élections municipales 2020 de Saint-Martin-d'Hères ( [voir activité de la semaine dernière](https://gitlab.com/simplon-dev-data/grenoble-2020-2021/prairie/-/tree/master/petit-dataset) ). Nous allons suivre les étapes suivantes :

- [ ] Récupérer le fichier CSV de données des élections municipales sur le [lien suivant](https://entrepot.metropolegrenoble.fr/opendata/38421-SMH/elections-resultats/2020/Municipales-2020-1er-tour.csv)

- [ ] Créer une base de données SQLite nommée "municipales_SMH.db" à l'aide de la commande suivante :
```bash
sqlite3 municipales_SMH.db
```

- [ ] Créer une table pour pouvoir insérer les données du fichier CSV en complétant la syntaxe suivante (la syntaxe suivante ne contient que deux colonnes, à vous d'ajouter les colonnes manquantes) :
```sql
sqlite> CREATE TABLE election (id INTEGER PRIMARY KEY, bureaux TEXT);
```

- [ ] Vérifier que la table est bien créée à l'aide la syntaxe suivante :
```
sqlite> .tables
```

- [ ] Insérer les 5 premières lignes du fichier CSV en complétant la syntaxe SQL suivante (la syntaxe suivante ne contient que deux colonnes, à vous d'ajouter les colonnes manquantes) :
```sql
sqlite> INSERT INTO election VALUES(1, '0001 - Mairie');
```

- [ ] Vérifier que les 5 premières lignes du fichier CSV ont bien été ajoutées dans la table en les affichant :
```sql
sqlite> SELECT * FROM election;
```

- [ ] En utilisant la fonction .header de SQLite, ajouter les noms de colonnes lors de l'affichage des données

- [ ] En utilisant la fonction .mode de SQLite, améliorer la mise en page des données

- [ ] Afficher uniquement les élèments de la colonne "bureaux"

- [ ] A l'aide de la fonction SQL d'aggrégation COUNT(), vérifier qu'il y a bien 5 lignes dans la table

## Activité sauvegarde de la structure et des données de la table dans sqlite3

Nous allons maintenant créer une sauvegarde (dump) de la structure et des données ajoutées dans la table election.

- [ ] Afficher le code SQL de la table election en utilisant la syntaxe suivante :
```sql
sqlite> .dump election
```

- [ ] Envoyer le résultat de la fonction .dump dans un fichier que l'on va nommer "election.sql" à l'aide de la syntaxe suivante :
```sql
sqlite> .output election.sql
sqlite> .dump election
sqlite> .output stdout
```

- [ ] Vérifier que le fichier "election.sql" a bien été créé avec les données de la table election

- [ ] Supprimer la table election de la base de données à l'aide de la syntaxe suivante :
```sql
sqlite> DROP TABLE election;
```

- [ ] Vérifier que la table election n'existe plus à l'aide de la syntaxe vue au début de cette activité

- [ ] Récréer la table à l'aide du dump "election.sql" à l'aide de la syntaxe suivante :
```sql
sqlite> .read election.sql
```

- [ ] Vérifier que la table election existe à nouveau et contient des données à l'aide des syntaxes vues au début de cette activité

## Activité sauvegarde de la structure et des données de la base de données en lignes de commande

Nous allons maintenant créer une sauvegarde (dump) de la structure et des données ajoutées dans l'**ensemble de la base de données** "municipales_SMH.db" directement en lignes de commande.

- [ ] Quitter sqlite3

- [ ] Créer le dump en ligne de commande à l'aide de la commande suivante :
```bash
sqlite3 municipales_SMH.db .dump > municipales.sql
```

- [ ] Vérifier le contenu du fichier "municipales.sql" en lignes de commande :
```bash
cat municipales.sql
```

- [ ] Supprimer la base de données "municipales_SMH.db" à l'aide de la commande suivante :
```bash
rm municipales_SMH.db
```

- [ ] Recréer la base de données à l'aide du dump créé précédemment grâce à la ligne de commande suivante :
```bash
sqlite3 municipales_SMH.db < municipales.sql	
```

- [ ] Ouvrir la base de données "municipales_SMH.db"

- [ ] Vérifier qu'elle contient la table election

- [ ] Vérifier les données de la table election

- [ ] Supprimer la base de données "municipales_SMH.db" en lignes de commande

## Activité création d'une table à partir d'un fichier CSV

Dans cette activité, on va créer directement une table à partir du fichier CSV des élections municipales. Pour cela, nous allons utiliser la fonction .import de SQLite.

- [ ] Créer une base de données SQLite nommée "resultats.db"

- [ ] Créer une table election à partir du fichier CSV grâce à la syntaxe suivante :
```sql
sqlite> .import Municipales-2020-1er-tour.csv election
```

- [ ] Vérifier que la table election a été créée

- [ ] Vérifier le contenu de la table election

- [ ] Compter le nombre de lignes dans la table election

- [ ] Créer un dump de la base de données "resulats.db"

- [ ] Vérifier le contenu du dump

- [ ] Supprimer la base de données "resultats.db" en lignes de commande

- [ ] Recréer la base de données à partir du dump