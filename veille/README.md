# Activité de veille technologique sur le workflow de la data 

En binôme, vous allez analyser un article sur un sujet lié aux données.

---

- **Sujet N°1 : démocratisation de la data chez airbnb**

[Article en anglais](https://blog.getcensus.com/how-airbnb-achieves-data-democratization-to-empower-5k-employees//?utm_source=reddit&utm_medium=post&utm_campaign=airbnbuniversity)

Lien vers le document de présentation :

https://docs.google.com/presentation/d/1Sr8VM9LtwNh1wUxxgl_OFCuFnTZWoGNOjoSK67QCrJA/edit?usp=sharing

Mots-clés : airbnb, data democratization, data university, SQL, Data driven decision

---

- **Sujet N°2 : le DataOps**

[Article en français](https://www.lemagit.fr/conseil/Quest-ce-que-le-DataOps-Data-Operations)

Lien vers le document de présentation :https://docs.google.com/presentation/d/1p6fEd2HOXt5_EFJb7kFKEO_90D2g6NHpXDtHCeajIaE/edit#slide=id.ga3343d0de6_0_53

Mots-clés : DataOps, DevOps, Agilité, ML, IA

---

- **Sujet N°3 : Data as a service (DaaS)**

[Article en français](https://www.lebigdata.fr/data-as-a-service-definition)

Lien vers le document de présentation : https://docs.google.com/presentation/d/1m8FnzauJFLSdFElXJZhXxnWE1yhElwNHflUkD7qzaoE/edit?usp=sharing

Mots-clés : cloud, hébergeur, gestion à distance, Big Data 


---

- **Sujet N°4 : l'intégrité des données**

[Article en anglais](https://www.technologynetworks.com/informatics/articles/what-is-data-integrity-343068)

Lien vers le document de présentation : 
https://docs.google.com/presentation/d/1kjk0d2K-ElA9pIhaKS1_Ill2uiO9ZCI54vkhmy_ZsCo/edit?usp=sharing

Mots-clés : alcoa, intégrité des données, sécurité des données

---

- **Sujet N°5 : les data swamps**

[Article en anglais](https://www.information-age.com/data-swamp-data-lake-123481597/)

Lien vers le document de présentation :https://docs.google.com/presentation/d/1q4vZnVOy5CwTZfRo85uRDWYVjygdkce01X4JMrMQDrE/edit#slide=id.gad74b3f0b8_0_31

Mots-clés : data lake, data swamp, data governance, automatisation, metadata

---

- **Sujet N°6 : le data steward**

[Article en français](https://www.oracle.com/fr/database/definition-data-steward.html)

Lien vers le document de présentation :https://docs.google.com/presentation/d/1pvqtZ8t7e67cy8pEA6NUSxJrc_lciskw7pggSf-AJ7g/edit#slide=id.gad6bfb4cef_0_49

Mots-clés : Assurer, Garantir la Qualité, Veiller, Communiquer, Vérifier

---

- **Sujet N°7 : ELT vs ETL**

[Article en anglais](https://www.softwareadvice.com/resources/etl-vs-elt-for-your-data-warehouse/)

Lien vers le document de présentation :https://docs.google.com/presentation/d/1K-FaJax4fC7a6PQzs57-8W0nH56TN5VLx7r-FYjrroM/edit?usp=sharing

Mots-clés : architecture, stockage données, volume de données, zone de transit, data warehouse

---

- **Sujet N°8 : erreur dans les données du COVID en Grande-Bretagne**

[Article en français](https://www.leparisien.fr/societe/covid-19-comment-50000-cas-contacts-ont-disparu-des-radars-a-cause-d-excel-en-grande-bretagne-06-10-2020-8397645.php)

Lien vers le document de présentation : https://docs.google.com/presentation/d/1O50-c_vMKgu2ZZbBrc_BrTC6AU8JD6eri1e_bIvrpoE/edit#slide=id.ga338c32bc8_2_1733

Mots-clés : Covid 19, Cas contact, Erreur, Excel, Britannique, Format

---

- **Sujet N°9 : la place de la data chez Palantir**

[Article en anglais](https://medium.com/palantir/palantir-is-not-a-data-company-palantir-explained-1-a6fcf8b3e4cb)

Lien vers le document de présentation : https://drive.google.com/file/d/1ZSS7nnh6KXyT6A18zFRQ5JO3bfhPP4rl/view?usp=sharing

Mots-clés : Information, démocratisation, sécurité, sous traitance, Mondial

---

## TODO : synthèse de l'article

- Préparer une présentation de 2 ou 3 minutes sur les idées principales contenues dans l'article étudié.
- Extraire les mots-clés liés aux données présents dans l'article (de 3 à 5 mots-clés)



Lien vers la liste des mots-clés
https://docs.google.com/document/d/1uuLfjXA8m33rOKMtrcIj1j6sboVribVsAMPQ9d8zlUg/edit?usp=sharing