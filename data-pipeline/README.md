# Mise en place d'un pipeline de données

Nous allons apprendre à mettre en place un premier pipeline de données simple.

Le but est d'automatiser les différentes étapes :

- Extraction de données
- Chargement de données
- Transformation de données

Nous utiliserons les technologies et outils suivants :

- Langage de script Shell
- Langage de données SQL
- Base de données SQLite

Le jeu de données utilisé est le relevé temps-réel de la consommation énergétique nationale,
selon l'application éCO2mix. Ce jeu de données est mis à jour toutes les heures et garde
un historique d'un mois.

URL : https://opendata.reseaux-energies.fr/explore/dataset/eco2mix-national-tr/information/

## Modalités

Cette activité est **à réaliser individuellement**, mais en **réfléchissant en groupe** :

- Chaque apprenant doit compléter ce document sur sa propre branche nommée `prenom-nom`
- Penser à cocher les cases à chaque étape réalisée
- Compléter chaque emplacement marqué `[A COMPLETER]`
- Compléter et/ou ajouter chaque fichier annexe nécessaire

## Partie 1 : extraction de données

Récupérons notre jeu de données brutes :

- [ ] Compléter la première partie du script Shell [`pipeline.sh`](pipeline.sh)
- [ ] Exécuter le pipeline avec la commande: `bash pipeline.sh`
- [ ] Vérifier que le jeu de données est bien téléchargé la commande Shell `head`

### Observations

[A COMPLETER]

## Partie 2 : chargement de données

Chargeons en base notre jeu de données brutes :

- [ ] Compléter le script SQL [`load.sql`](load.sql)
- [ ] Compléter la seconde partie du script Shell [`pipeline.sh`](pipeline.sh)
- [ ] Exécuter le pipeline `pipeline.sh`
- [ ] Vérifier que le jeu de données brutes est bien chargé en base dans l'invite de commande `sqlite3`

### Observations

[A COMPLETER]

## Partie 3 : transformation de données

Transformons notre jeu de données brutes en données propres :

- [ ] Compléter le script SQL [`transform.sql`](transform.sql)
- [ ] Compléter la troisième partie du script Shell [`pipeline.sh`](pipeline.sh)
- [ ] Exécuter le pipeline `pipeline.sh`
- [ ] Vérifier que le jeu de données propres est bien chargé en base dans l'invite de commande `sqlite3`
- [ ] Exécuter la première requête [`query.sql`](query.sql) avec votre base de données. Le résultat doit avoir la forme suivante :
```bash
heure|total_conso
2020-12-01 21:00:00|0
2020-12-01 20:00:00|263972
2020-12-01 19:00:00|280080
2020-12-01 18:00:00|296465
2020-12-01 17:00:00|294098
2020-12-01 16:00:00|284156
2020-12-01 15:00:00|274233
2020-12-01 14:00:00|275250
...
```

### Observations

[A COMPLETER]

## Partie 4 : automatisation du pipeline

Afin de que notre pipeline de données puisse s'exécuter automatiquement
à intervalle de temps régulier (par exemple, toutes les heures),
nous allons utiliser le programme `cron`.

- [ ] Installer `cron`:
```bash
sudo apt install cron
```
- [ ] Compléter le planning Cron dans le fichier [`cronjob`](cronjob) pour que le pipeline s'exécute toutes les minutes
- [ ] Ajouter le contenu du fichier `cronjob` dans l'invite: `crontab -e`
- [ ] Observer régulièrement le jeu de données propres avec la requête `query.sql`

### Observations

[A COMPLETER]
