#!/bin/bash

# Ce script automatise un pipeline de données.
#
# Utilisation:
#   ./pipeline.sh

DATA_URL="https://opendata.reseaux-energies.fr/explore/dataset/eco2mix-national-tr/download/?format=csv&timezone=Europe/Berlin&lang=fr&use_labels_for_header=true&csv_separator=,"
DATA_FILE=data.csv
DATABASE_FILE=data.db

##############################################################################
# Partie 1 : extraction de données
##############################################################################

echo "Téléchargement du dataset ${DATA_FILE} (${DATA_URL})"

##############################################################################
# Partie 2 : chargement de données
##############################################################################

echo "Chargement des données en base ${DATABASE_FILE} (load.sql)"

##############################################################################
# Partie 3 : transformation de données
##############################################################################

echo "Transformation des données en base ${DATABASE_FILE} (transform.sql)"

##############################################################################
# nettoyage des fichiers temporaires
##############################################################################

echo "Nettoyage des fichiers temporaires (${DATA_FILE})"
rm ${DATA_FILE}
