-- Consommation globale sur les dernières 24 heures, heure par heure

SELECT
    strftime('%Y-%m-%d %H:00:00', date_heure) as heure,
    SUM(consommation) as total_conso
FROM consommation
WHERE datetime(date_heure) BETWEEN datetime('now', '-24 hours') AND datetime('now')
GROUP BY heure
ORDER BY heure DESC;
