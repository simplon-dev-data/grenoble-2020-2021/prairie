-- 1. Créer la table des données propres "consommation" si elle n'existe pas
-- Indice : regarder comment est utilisée la table "consommation" pour trouver
--          les intitulés et les types des colonnes ...

CREATE TABLE IF NOT EXISTS consommation (
    ...
);

-- 2. Vider la table des données propres "consommation"

... consommation;

-- 3. Seléctionner, transformer et importer les données uniques depuis "raw_data" dans "consommation"
-- Indice : inspecter la table "raw_data" pour trouver les intitulés des colonnes sources ...

INSERT INTO consommation
SELECT DISTINCT
    ...
FROM raw_data;
