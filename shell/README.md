# Activité Shell

Nous souhaitons pouvoir récupérer un jeu de données en passant par une ligne de commande,
afin de pouvoir automatiser cette récupération au cours du temps.

## Modalités

Cette activité est **à réaliser individuellement**, mais en **réfléchissant en groupe** :

- Chaque apprenant doit compléter ce document sur sa propre branche nommée `prenom-nom`
- Penser à cocher les cases à chaque étape réalisée
- Compléter chaque emplacement marqué `[A COMPLETER]`

## Partie 1 : recherche d'information

- [ ] Explorer le web afin de trouver un programme en ligne de commande Linux
permettant de télécharger un fichier plat dans le dossier courant

- [ ] Tester le(s) programme(s) trouvé(s). Par exemple pour télécharger un jeu de données CSV:

```bash
<nom-du-programme> https://entrepot.metropolegrenoble.fr/opendata/38421-SMH/scolaire/groupe-scolaire-2020.csv
```

- [ ] Echanger vos trouvailles avec votre groupe, et comparer les avantages
et inconvénients de chaque programme trouvé

### Observations

[A COMPLETER]

## Partie 2 : mise en application

- [ ] En utilisant le programme trouvé à l'étape précédente, remplir le script Shell
[data_script.sh](./data_script.sh) avec la commande adéquate

- [ ] Votre script doit pouvoir fonctionner tel que:

```bash
./data_script.sh <url> <fichier-destination>
```

- [ ] Vérifier que le fichier de destination contient les données souhaitées

### Observations

[A COMPLETER]

## Partie 3 : sauvegarde

Une fois votre script complété:

- [ ] Créer / basculer sur votre branche sur ce dépôt Git nommée `<prenom>-<nom>`
- [ ] Commiter le script Shell `data_script.sh`
- [ ] Pousser les changements de votre branche `<prenom>-<nom>` sur GitLab

### Observations

[A COMPLETER]
