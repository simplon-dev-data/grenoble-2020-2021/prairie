# Analyse d'un petit jeu de données

Pour le début de la prairie, nous allons commencer par analyser un **petit jeu de données**. 
L'activité va porter sur un jeu de données correspondant aux résultats du premier tour des élections municipales de 2020 à Saint-Martin-d'Hères. Vous pouvez télécharger le fichier de données sur ce lien : [Elections municipales 2020 à Saint Martin d'Hères](https://entrepot.metropolegrenoble.fr/opendata/38421-SMH/elections-resultats/2020/Municipales-2020-1er-tour.csv). L'activité sera réalisée **individuellement**. 

Pour cette activité, vous devez créer votre propre branche nommée `prenom-nom` pour tracer votre avancement et répondre aux questions.

## L'activité : synthèse du jeu de données

- [ ] Télécharger le fichier de données
- [ ] Utiliser un outil pour en afficher le contenu

*Etant donné la taille du jeu de données, un tableur peut être utilisé (Google Sheets, Libre Office Calc, Excel...)*

- [ ] Présenter une synthèse des données (par exemple, trouver 3 ou 4 nombres représentatifs du jeu de données)

*On peut synthétiser le jeu de données par le **nombre de colonnes** et le **nombre total de valeurs** dans le jeu de données*

- [ ] Classer les colonnes de votre jeu de données en deux types

*Données qualitatives et données quantitatives*

## QUESTIONS

- Le jeu de données vous semble-t-il propre ?
- Pensez-vous qu'il pourrait être amélioré en ajoutant d'autres données ? Si oui, lesquelles ?
- Sur le site source du jeu de données [Open data Grenoble](https://data.metropolegrenoble.fr/), quels sont les différents types de formats des jeux de données? Même question pour les jeux de données de la plateforme de diffusion des données publiques de l'état français [Data Gouv](https://www.data.gouv.fr/fr/)?

