# Exploration de données avec un tableur

Nous allons apprendre à explorer et exploiter des données à l'aide d'un tableur.
Les logiciels de tableur sont nombreux et tombent dans deux catégories :

- Desktop (Microsoft Excel, LibreOffice Calc)
- Cloud (Office365 Excel, Google Sheets)

## Modalités

Pour des raisons de praticité, nous allons utiliser le service gratuit
**Google Sheets** inclus avec tous les comptes Google

Cette activité est **à réaliser individuellement**, mais en **réfléchissant en binôme** :

- Chaque apprenant doit compléter ce document sur sa propre branche nommée `prenom-nom`
- Penser à cocher les cases à chaque étape réalisée
- Compléter chaque emplacement marqué `[A COMPLETER]`

## Jeu de données

Le jeu de données que nous allons utiliser répertorie les points d'apport
volontaires de la métropole de Grenoble.

URL : https://data.metropolegrenoble.fr/ckan/dataset/points-d-apports-volontaire/resource/9be5666b-f050-4584-b87e-e937e5be080a

Nous cherchons à répondre aux questions suivantes à minima :

- Quel est le nombre de conteneur par secteur ?
- Quel est le nombre de conteneur par commune ?
- Quelle est la répartition de type de conteneur par type de déchet ?
- Quels conteneurs sont hors-service ?
- Cartographie géographique des emplacements ?

## Partie 1 : Extraction des données

- [ ] Télécharger le jeu de données CSV
- [ ] Ouvrir et observer le jeu de données dans un éditeur de texte

### Observations

[A COMPLETER]

## Partie 2 : Chargement des données

- [ ] Créer un tableur Google Sheets
- [ ] Renommer le workbook en "SOURCE" et ajouter la source de donnée
- [ ] Importer fichier CSV en créant nouveau workbook
- [ ] Renommer le workbook importé en "RAW DATA"
- [ ] Utiliser le tableau en mode "filtre" pour explorer les données
- [ ] Afficher les statistiques des colonnes (total, cellules vides, valeurs uniques, etc.)

### Observations

[A COMPLETER]

## Partie 3 : Nettoyage des données

**Note** : avant et après chaque transformation, pensez à afficher les statistiques
des colonnes afin d'en valider l'effet !

- [ ] Dupliquer le workbook "RAW DATA" en "DATA"
- [ ] Utiliser le tableau en mode "filtre"
- [ ] Fixer les intitulés des colonnes sur la première ligne
- [ ] Supprimer les colonnes non intéressantes (identifiants spécifiques, redondances, etc.)
- [ ] Formatter les nombres
- [ ] Formatter les dates
- [ ] Supprimer les espaces inutiles
- [ ] Supprimer les doublons

### Observations

[A COMPLETER]

## Partie 4 : Exploitation des données

Pour cette dernière partie, le but est de pouvoir répondre aux questions énoncées
au début du sujet.

Pour chaque question à répondre :

- [ ] Créer une table pivot dans un nouveau workbook
- [ ] Renommer la/les colonnes de valeurs
- [ ] Insérer un graphique à partir de la table pivot
- [ ] Renommer le graphique avec un titre explicite
- [ ] Télécharger le workbook courant au format PDF

### Observations

[A COMPLETER]

## Partie 5 : Bonus (optionnel)

Si vous souhaitez aller plus loin, vous pouvez imaginer d'autres questions
à partir du jeu de données propre et tenter d'y répondre de la même manière
que la partie 4 !

## Conclusion

En binôme, préparer une restitution d'activité de 3 minutes maximum.

Quelques axes de réflexion :

- Qu'avez-vous appris à l'issue de cette activité ?
- Quels sont les avantages d'un tableur ?
- Quels sont les inconvénients d'un tableur ?
- Quels sont les limites d'un tableur ?

[A COMPLETER]
